//
//  BoxContainerViewControllerTests.swift
//  BoxTests
//
//  Created by Raimundas Sakalauskas on 11/04/2018.
//  Copyright © 2018 Raimundas Sakalauskas. All rights reserved.
//

import XCTest
@testable import Box

class BoxContainerViewControllerTests: XCTestCase {
    
    private var vc: BoxContainerViewController?
    
    override func setUp() {
        super.setUp()
        
        vc = (UIStoryboard.init(name: "Main", bundle: Bundle.main)
            .instantiateViewController(withIdentifier: "BoxContainerViewController") as! BoxContainerViewController)
        vc!.loadView()
    }
    
    override func tearDown() {
        vc = nil
        
        super.tearDown()
    }
    
    func testOutletsNotNil() {
        XCTAssertNotNil(vc)
    }
    
}
