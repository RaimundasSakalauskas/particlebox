//
//  BoxDocumentViewControllerTests.swift
//  BoxTests
//
//  Created by Raimundas Sakalauskas on 11/04/2018.
//  Copyright © 2018 Raimundas Sakalauskas. All rights reserved.
//

import XCTest
@testable import Box

class BoxDocumentViewControllerTests: XCTestCase {
    
    private var vc: BoxDocumentViewController?
    
    override func setUp() {
        super.setUp()
        
        vc = (UIStoryboard.init(name: "Main", bundle: Bundle.main)
            .instantiateViewController(withIdentifier: "BoxDocumentViewController") as! BoxDocumentViewController)
        vc!.loadView()
    }
    
    override func tearDown() {
        vc = nil
        
        super.tearDown()
    }
    
    func testOutletsNotNil() {
        XCTAssertNotNil(vc)
        XCTAssertNotNil(vc!.scrollView)
        XCTAssertNotNil(vc!.keyLabel)
        XCTAssertNotNil(vc!.valueLabel)
        XCTAssertNotNil(vc!.deviceIdLabel)
        XCTAssertNotNil(vc!.updatedAtLabel)
        XCTAssertNotNil(vc!.productIdLabel)
        XCTAssertNotNil(vc!.scopeLabel)
        XCTAssertNotNil(vc!.backgroundView)
        XCTAssertNotNil(vc!.tipLabel)
    }
    
}
