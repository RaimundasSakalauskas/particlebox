//
//  BoxDocumentScopeListViewControllerTests.swift
//  BoxTests
//
//  Created by Raimundas Sakalauskas on 11/04/2018.
//  Copyright © 2018 Raimundas Sakalauskas. All rights reserved.
//

import XCTest
@testable import Box

class BoxDocumentScopeListViewControllerTests: XCTestCase {
    
    private var vc: BoxDocumentScopeListViewController?
    
    override func setUp() {
        super.setUp()
        
        vc = (UIStoryboard.init(name: "Main", bundle: Bundle.main)
            .instantiateViewController(withIdentifier: "BoxDocumentScopeListViewController") as! BoxDocumentScopeListViewController)
        vc!.loadView()
    }
    
    override func tearDown() {
        vc = nil
        
        super.tearDown()
    }
    
    func testOutletsNotNil() {
        XCTAssertNotNil(vc)
        XCTAssertNotNil(vc!.tableView)
    }
    
}

    

