//
//  APIClientTests.swift
//  BoxTests
//
//  Created by Raimundas Sakalauskas on 07/04/2018.
//  Copyright © 2018 Raimundas Sakalauskas. All rights reserved.
//

import XCTest
@testable import Box

class APIClientTests: XCTestCase {

    var boxDocument:BoxDocument?

    override func setUp() {
        super.setUp()

        boxDocument = BoxDocument(key: "testhumidity", value: "test2",
                    deviceId: "test4321", updatedAt: Date(),
                    scope: .device, productId: 4321)
    }
    
    override func tearDown() {
        super.tearDown()

        boxDocument = nil
    }
    
    func testListBoxesWithNoParameters() {
        let expectation = XCTestExpectation(description: "Download box document list from API")
        
        APIClient.instance.listBoxes { (response) in
            //do nothing
            switch response {
                case .success(_, _):
                    XCTAssert(true)
                case .failure(_, _):
                    XCTAssert(false)
            }

            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10)
    }
    
    func testBoxDocumentUploadAndDelete() {
        let expectation = XCTestExpectation(description: "Upload box document to API and then remove it")

        uploadBox { uploadSuccess in
            if (!uploadSuccess) {
                XCTAssert(false)
                expectation.fulfill()
            } else {
                self.deleteBox { deleteSuccess in
                    if (!deleteSuccess) {
                        XCTAssert(false)
                        expectation.fulfill()
                    } else {
                        XCTAssert(true)
                        expectation.fulfill()
                    }
                }
            }
        }

        wait(for: [expectation], timeout: 10)
    }
    
    func testBoxDocumentUploadQueryAndDelete() {
        let expectation = XCTestExpectation(description: "Upload box document to API, query it and then remove it")
        
        uploadBox { uploadSuccess in
            if (!uploadSuccess) {
                XCTAssert(false)
                expectation.fulfill()
            } else {
                self.getBox { getSuccess in
                    if (!getSuccess) {
                        XCTAssert(false)
                        expectation.fulfill()
                    } else {
                        self.deleteBox { deleteSuccess in
                            if (!deleteSuccess) {
                                XCTAssert(false)
                                expectation.fulfill()
                            } else {
                                XCTAssert(true)
                                expectation.fulfill()
                            }
                        }
                    }
                }
            }
        }
        
        wait(for: [expectation], timeout: 10)
    }

    func testBoxDocumentUploadQueryAndDeleteTwice() {
        let expectation = XCTestExpectation(description: "Upload box document to API, query it and then remove it. Then try to remove it once more which should fail.")
        
        uploadBox { uploadSuccess in
            if (!uploadSuccess) {
                XCTAssert(false)
                expectation.fulfill()
            } else {
                self.getBox { getSuccess in
                    if (!getSuccess) {
                        XCTAssert(false)
                        expectation.fulfill()
                    } else {
                        self.deleteBox { deleteSuccess in
                            if (!deleteSuccess) {
                                XCTAssert(false)
                                expectation.fulfill()
                            } else {
                                self.deleteBox { deleteTwiceSuccess in
                                    if (deleteTwiceSuccess) {
                                        //note the condition different from other filters
                                        //because now webservice should return error code
                                        //as in this test we are trying to delete same box
                                        //twice
                                        XCTAssert(false)
                                        expectation.fulfill()
                                    } else {
                                        XCTAssert(true)
                                        expectation.fulfill()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        wait(for: [expectation], timeout: 10)
    }
    
    private func deleteBox(_ closure: @escaping (Bool) -> ()) {
        APIClient.instance.deleteBox(boxDocument: boxDocument!) { (response) in
            switch response {
            case .success(_, _):
                closure(true)
            case .failure(_, _):
                closure(false)
            }
        }
    }

    private func uploadBox(_ closure: @escaping (Bool) -> ()) {
        APIClient.instance.uploadBox(boxDocument: boxDocument!) { (response) in
            switch response {
            case .success(_, _):
                closure(true)
            case .failure(_, _):
                closure(false)
            }
        }
    }
    
    private func getBox(_ closure: @escaping (Bool) -> ()) {
        APIClient.instance.getBox(boxDocument: boxDocument!) { (response) in
            switch response {
            case .success(_, _):
                closure(true)
            case .failure(_, _):
                closure(false)
            }
        }
    }



}


