//
//  ListViewControllerTests.swift
//  BoxTests
//
//  Created by Raimundas Sakalauskas on 09/04/2018.
//  Copyright © 2018 Raimundas Sakalauskas. All rights reserved.
//

import XCTest
@testable import Box

class BoxListViewControllerTests: XCTestCase {
    
    private var vc: BoxListViewController?
    
    override func setUp() {
        super.setUp()
        
        vc = (UIStoryboard.init(name: "Main", bundle: Bundle.main)
                .instantiateViewController(withIdentifier: "BoxListViewController") as! BoxListViewController)
        vc!.loadView()
    }
    
    override func tearDown() {
        vc = nil
        
        super.tearDown()
    }
    
    func testOutletsNotNil() {
        XCTAssertNotNil(vc)
        XCTAssertNotNil(vc!.tableView)
        XCTAssertNotNil(vc!.noResultsView)
        XCTAssertNotNil(vc!.noBoxesView)
        XCTAssertNotNil(vc!.footerView)
        XCTAssertNotNil(vc!.tableViewBottomConstraint)
        
    }
}
