# Particle Box API Demo App 

This app is developed to illustrate iOS skills. 

App video can be seen [here](https://www.youtube.com/watch?v=U-BQmqCXCM8)

### Running the app

It is very simple but here are the instruction:  
1) Clone / Download repository  
2) Run "pod install"  
3) Add a valid base url and access token in APIClient class  
4) Run the app

Settings class contains 2 properties, that allows see paging in action with mockup API (that has static response).

Please be sure to check what hides when you start with the fresh list or delete all the items from the list :) (unless you saw the video :))

## Considerations (from most important to least important)

This is demo app and though I would love to polish it to be perfect, I suppose it's not the exact purpose of this project. Also, given I didn't want to disturb you with not-so-important questions (such as if paging starts at page 0 or at page 1 and if amount of pages is 0 based or 1 based), I decided to add these considerations and cut few corners to save some time and deliver sooner.

### iOS 11 requirement

App is built against iOS 11 SDK. It could easily be built against iOS 9 SDK by using custom DateFormater in APIClient class instead of ISO8601DateFormatter (requires iOS 10) with .withFractionalSeconds option (requires iOS 11). Also 4s screen size would require some adjustments in some views, because iPhone 5 was smallest form factor I tested on.

### iPhone only support

App is now set to support iPhone only (and portrait only). I have disabled landscape from UX point of view, because I don't think it's useful for such app and its an extra thing to maintain in the future (but since I used autolayout, it should work without any further coding if reenabled). As for iPad, to support it on technical level only popoverPresentationController sourceView/Rect would need to be defined, but if this was a production app, I would use different UX approach for it as well.

### Paging

I assume that totalPages is 1 based such if there are 100 elements 10 pages each, total pages will be 10 and page starts at 1. The most sensitive part is deleting the box document. In order for further paging & content to be correct, when one cell is deleted, I load & add one document to the end of the list. This way if user scrolls to the end of the list and next page should be loaded, all content should stay correct. 

### Cell content

Since there was little to none explanations behind the API model class, I had to make assumptions. My assumption is that product id could possibly be used to map to product name which would possibly be better for the user, but since I don't have access to such mapping, I left it visible in the cell. Scope could potentially be used with different color icons, but for demo proposes used only the letters U, P and D to differentiate the scope.

### Unit tests

I have added the bare minimum of unit tests. I only test for working API endpoints and view controller outlets. Given that UI can be manually tested in less than 1 minute, I don't think UI tests would be necessary for such app even in production. 

### LocalizedString

This approach works best if there's a way to switch language within the app. It would have to be altered a little to use device language.

### RefreshControl

Native RefreshControl is really awful. If I had enough time, I would work on custom control that would provide butter-smooth animations. Since that would take some time I added MBProgressHUD to the project to cover the scenarios where refreshControl fails (initial data loading & relativly long load after item is deleted).

### Visual stuff

UI misses the instruction message that would not intruisivly suggest that "pull down to refresh" behavior is available in list & box views.

## Author

Raimundas Sakalauskas

Email: [raimundas.sakalauskas@gmail.com](raimundas.sakalauskas@gmail.com)  
Phone: +370 671 07695