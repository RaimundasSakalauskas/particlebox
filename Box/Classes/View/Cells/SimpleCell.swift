//
// Created by Raimundas Sakalauskas on 11/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation

class SimpleCell : UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!

    class var nibName: String {
        return "SimpleCellView"
    }

    class var cellIdentifier: String {
        return "SimpleCell"
    }

    class var height: CGFloat {
        return 45
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setup(titleKey: String) {
        self.titleLabel.text = titleKey.localized()
    }
}
