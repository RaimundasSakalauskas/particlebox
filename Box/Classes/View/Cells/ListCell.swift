//
// Created by Raimundas Sakalauskas on 11/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation

class ListCell : SimpleCell {

    @IBOutlet weak var valueLabel: UILabel!


    override class var nibName: String {
        return "ListCellView"
    }

    override class var cellIdentifier: String {
        return "ListCell"
    }

    override class var height: CGFloat {
        return 75
    }

    func setup(titleKey: String, value: String) {
        super.setup(titleKey: titleKey)
        self.valueLabel.text = value
    }
}
