//
// Created by Raimundas Sakalauskas on 10/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation
import UIKit

class LoadingCell : UITableViewCell {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    class var nibName: String {
        return "LoadingCellView"
    }

    class var cellIdentifier: String {
        return "LoadingCell"
    }

    class var height: CGFloat {
        return 65
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        activityIndicator.color = UIColor.BoxColor.blueDark
    }
}
