//
// Created by Raimundas Sakalauskas on 09/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation
import UIKit


class BoxDocumentCell: UITableViewCell {

    @IBOutlet weak var keyLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var deviceIdLabel: UILabel!
    @IBOutlet weak var updatedAtLabel: UILabel!
    @IBOutlet weak var productIdLabel: BlackLabel!
    
    @IBOutlet weak var iconView: UIView!
    @IBOutlet weak var scopeLabel: UILabel!
    
    @IBOutlet weak var scopeBackground: SelfColoringView!
    
    class var nibName: String {
        return "BoxDocumentCellView"
    }

    class var cellIdentifier: String {
        return "BoxDocumentCell"
    }

    class var height: CGFloat {
        return 120
    }

    func setup(boxDocument: BoxDocument) {
        keyLabel.text = boxDocument.key.capitalized
        valueLabel.text = boxDocument.value

        if let deviceId = boxDocument.deviceId {
            deviceIdLabel.text = deviceId
        } else {
            updatedAtLabel.text = LocalizedString.BoxDocument.Unknown.localized()
        }

        if let date = boxDocument.updatedAt {
            updatedAtLabel.text = ((date as NSDate).timeAgo() as String).capitalized
        } else {
            updatedAtLabel.text = LocalizedString.BoxDocument.Unknown.localized()
        }

        if let productId = boxDocument.productId {
            productIdLabel.text = String(productId)
        } else {
            productIdLabel.text = LocalizedString.BoxDocument.Unknown.localized()
        }

        if let scope = boxDocument.scope {
            switch scope {
                case .device:
                    scopeLabel.text = "D"
                case .user:
                    scopeLabel.text = "U"
                case .product:
                    scopeLabel.text = "P"
            }
        } else {
            scopeLabel.text = "-"
        }


        iconView.layer.masksToBounds = true
        iconView.layer.cornerRadius = 5.0
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()

        keyLabel.text = ""
        valueLabel.text = ""
        
        deviceIdLabel.text = ""
        updatedAtLabel.text = ""
        productIdLabel.text = ""

        scopeLabel.text = ""
    }

    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)

        scopeBackground.reset()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        scopeBackground.reset()
    }
}
