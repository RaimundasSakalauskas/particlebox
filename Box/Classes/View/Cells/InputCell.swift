//
// Created by Raimundas Sakalauskas on 11/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation

class InputCell : SimpleCell {
    
    @IBOutlet weak var inputText: UITextField!

    
    override class var nibName: String {
        return "InputCellView"
    }

    override class var cellIdentifier: String {
        return "InputCell"
    }

    override  class var height: CGFloat {
        return 85
    }

    func setup(titleKey: String, placeholderKey: String) {
        super.setup(titleKey: titleKey)
        self.inputText.placeholder = placeholderKey.localized()
    }
}
