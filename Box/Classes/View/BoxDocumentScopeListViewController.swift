//
// Created by Raimundas Sakalauskas on 11/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation

class BoxDocumentScopeListViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!


    private var callback: ((BoxDocumentScope?) -> ())!
    private var currentSelection: BoxDocumentScope?

    // MARK: Setup
    func setup(currentScope: BoxDocumentScope?, callback: @escaping (BoxDocumentScope?) -> ()) {
        self.callback = callback
        self.currentSelection = currentScope
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupTableView()
    }


    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self

        tableView.estimatedSectionHeaderHeight = 50
        tableView.sectionHeaderHeight = 50

        tableView.estimatedRowHeight = SimpleCell.height
        tableView.rowHeight = SimpleCell.height

        tableView.separatorColor = UIColor.BoxColor.greySeparator
        tableView.backgroundColor = UIColor.BoxColor.greyBackground

        tableView.register(UINib.init(nibName: SimpleCell.nibName, bundle: Bundle.main),
                forCellReuseIdentifier: SimpleCell.cellIdentifier)
    }

    // MARK: Display
    override func willMove(toParentViewController parent: UIViewController?) {
        super.willMove(toParentViewController: parent)

        if (parent == nil) {
            callback(currentSelection)
        }
    }

    // MARK: TableView Data Source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return BoxDocumentScope.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell:SimpleCell! = tableView.dequeueReusableCell(withIdentifier: SimpleCell.cellIdentifier, for: indexPath) as! SimpleCell
        cell.accessoryType = .checkmark
        cell.tintColor = .clear
        cell.setup(titleKey: BoxDocumentScope(int: indexPath.row)!.rawValue.capitalized)

        if (currentSelection?.toInt() == indexPath.row) {
            cell.tintColor = UIColor.BoxColor.blueDark
        }

        return cell
    }


    // MARK: TableView Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)


        if let currentSelection = currentSelection {
            tableView.cellForRow(at: IndexPath(row: currentSelection.toInt(), section: 0))?.tintColor = .clear
        }

        if (currentSelection != BoxDocumentScope(int: indexPath.row)) {
            currentSelection = BoxDocumentScope(int: indexPath.row)
            tableView.cellForRow(at: IndexPath(row: currentSelection!.toInt(), section: 0))?.tintColor = UIColor.BoxColor.blueDark
        } else {
            currentSelection = nil
        }

        callback(currentSelection)
    }
}
