//
// Created by Raimundas Sakalauskas on 11/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation

class ContainerViewController: UIViewController, UIGestureRecognizerDelegate {
    static var instance: ContainerViewController!

    var filterViewController: UIViewController!
    var filterXConstraint: NSLayoutConstraint!


    var panGestureRecognizer: UIPanGestureRecognizer!

    var fadeView: UIView!
    var animating: Bool = false

    var maxX: CGFloat = 0

    override func viewDidLoad() {
        ContainerViewController.instance = self

        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panGestureTriggered))
        panGestureRecognizer.delegate = self
        panGestureRecognizer.isEnabled = false
        view.addGestureRecognizer(panGestureRecognizer)

        setupFade()
        setupFilter()

        super.viewDidLoad()
    }

    @objc
    func panGestureTriggered(_ recognizer: UIPanGestureRecognizer) {
        switch (recognizer.state) {

        case .began:
            maxX = 0

        case .changed:
            let change = recognizer.translation(in: view)

            if (change.x > maxX) {
                maxX = change.x
            }

            var offset = maxX - change.x
            if (offset >= 320) {
                offset = 320

                panGestureRecognizer.isEnabled = false
                panGestureRecognizer.isEnabled = true
            }

            fadeView.alpha = (320.0 - offset) / 320.0
            filterXConstraint.constant = 320 * fadeView.alpha
        case .failed:
            fallthrough
        case .cancelled:
            fallthrough
        case .ended:
            let change = recognizer.translation(in: view)
            let velocity = recognizer.velocity(in: view)

            if (change.x > maxX) {
                maxX = change.x
            }

            let offset = maxX - change.x
            if (offset >= 160 || velocity.x > 250) {
                hideFilter()
            } else {
                showFilter(true)
            }
        case .possible:
            //do nothing
            break
        }
    }

    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return !animating
    }

    private func setupFade() {
        fadeView = UIView()

        fadeView.translatesAutoresizingMaskIntoConstraints = false
        fadeView.backgroundColor = UIColor.black.withAlphaComponent(0.5)

        view.addSubview(fadeView)

        fadeView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        fadeView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        fadeView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        fadeView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideFilter))
        tapGestureRecognizer.isEnabled = true
        tapGestureRecognizer.numberOfTouchesRequired = 1
        fadeView.addGestureRecognizer(tapGestureRecognizer)

        fadeView.isHidden = true
    }



    private func setupFilter() {
        filterViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BoxFilterViewController")

        let vcView = filterViewController.view!
        vcView.translatesAutoresizingMaskIntoConstraints = false

        addChildViewController(filterViewController)
        view.addSubview(vcView)

        vcView.widthAnchor.constraint(equalToConstant: 320).isActive = true
        vcView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        vcView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        filterXConstraint = vcView.rightAnchor.constraint(equalTo: view.leftAnchor)
        filterXConstraint.isActive = true

        vcView.isHidden = true
    }

    func showFilter(_ skipAppearanceEvents: Bool = false) {
        fadeView.isHidden = false

        //if scope view is visible, return to root view
        (filterViewController as! UINavigationController).popToRootViewController(animated: false)

        filterViewController.view.isHidden = false
        panGestureRecognizer.isEnabled = true

        view.setNeedsLayout()
        view.layoutIfNeeded()

        let duration: Double = (320 - Double(filterXConstraint.constant)) / 320 * 0.25

        filterXConstraint.constant = 320
        if (!skipAppearanceEvents) {
            filterViewController.beginAppearanceTransition(true, animated: true)
        }

        animating = true

        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.layoutIfNeeded()
            self.fadeView.alpha = 1
        }, completion: { b in
            if (!skipAppearanceEvents) {
                self.filterViewController.endAppearanceTransition()
            }
            self.animating = false
        })
    }

    @objc
    func hideFilter() {
        let duration: Double = (Double(filterXConstraint.constant) / 320) * 0.25

        filterXConstraint.constant = 0
        filterViewController.beginAppearanceTransition(true, animated: true)
        panGestureRecognizer.isEnabled = false

        animating = true

        UIView.animate(withDuration: duration, animations: { () -> Void in
            self.view.layoutIfNeeded()
            self.fadeView.alpha = 0
        }, completion: { b in
            self.filterViewController.endAppearanceTransition()
            self.fadeView.isHidden = true
            self.filterViewController.view.isHidden = true
            self.animating = false

            NotificationCenter.default.post(name: .ViewShouldBeReloaded, object: nil)
        })
    }


}
