//
// Created by Raimundas Sakalauskas on 10/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController: UIViewController, MBProgressHUDDelegate, UITextFieldDelegate {

    private var loader: MBProgressHUD?
    private var tapGestureRecognizer: UITapGestureRecognizer!

    override func viewDidLoad() {
        super.viewDidLoad()

        //add gesture recognizer to listen for outside taps
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapGestureRecognizerTriggered))
        tapGestureRecognizer.isEnabled = false
        view.addGestureRecognizer(tapGestureRecognizer)

        navigationItem.title = navigationItem.title?.localized()
    }

    @objc
    func tapGestureRecognizerTriggered() {
        view.endEditing(true)
    }

    open func showLoader() {
        disable()

        if (loader == nil) {
            loader = MBProgressHUD.showAdded(to: view, animated: true)
            loader!.delegate = self
            loader!.mode = MBProgressHUDMode.indeterminate
            loader!.contentColor = UIColor.BoxColor.blueDark
            loader!.backgroundView.backgroundColor = UIColor.BoxColor.greyBackground.withAlphaComponent(0.90)

            loader!.bezelView.style = MBProgressHUDBackgroundStyle.solidColor
            loader!.bezelView.color = UIColor.clear
        }
    }

    open func hideLoader() {
        if let loader = loader {
            loader.hide(animated: true)
        }
    }

    open func hudWasHidden(_ hud: MBProgressHUD) {
        loader = nil
        enable()
    }

    open func disable() {
        view.endEditing(true)

        if let items =  navigationController?.navigationBar.topItem?.leftBarButtonItems {
            for item in items {
                item.isEnabled = false
            }
        }

        if let items =  navigationController?.navigationBar.topItem?.rightBarButtonItems {
            for item in items {
                item.isEnabled = false
            }
        }
    }

    open func enable() {
        if let items =  navigationController?.navigationBar.topItem?.leftBarButtonItems {
            for item in items {
                item.isEnabled = true
            }
        }

        if let items =  navigationController?.navigationBar.topItem?.rightBarButtonItems {
            for item in items {
                item.isEnabled = true
            }
        }
    }

    func showNetworkError(code: Int, message: String?,
                          retry: @escaping () -> (),
                          cancel: @escaping () -> ()) {
        let alert = UIAlertController(title: LocalizedString.Errors.NetworkError.Title.localized(),
                message: LocalizedString.Errors.NetworkError.Message.localized(), preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: LocalizedString.Actions.Retry.localized(),
                style: .default) { action in
            retry()
        })

        alert.addAction(UIAlertAction(title: LocalizedString.Actions.Cancel.localized(),
                style: .cancel) { action in
            cancel()
        })

        self.present(alert, animated: true)
    }

    // MARK: Textfield Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        var target: UIView? = nil
        var nextTarget = textField.tag + 1
        
        target = view.viewWithTag(nextTarget)
        while (target == nil) {
            nextTarget += 1
            target = view.viewWithTag(nextTarget)

            if (nextTarget > textField.tag + 20) {
                break
            }
        }

        var nextResponder: UIResponder?
        if let target = target, let superview = target.superview {
            if (!superview.isHidden) {
                nextResponder = target
            }
        }

        if let nextResponder = nextResponder {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            //lastTextfieldShouldReturnAction
        }
        
        return false
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
    }

    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }

    func removeKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
    }

    @objc
    open func keyboardWillShow(_ notification: Notification) -> CGFloat {
        tapGestureRecognizer.isEnabled = true
        let userInfo = notification.userInfo!
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let size = keyboardScreenEndFrame.size.height
        return size
    }

    @objc
    open func keyboardWillHide(_ notification: Notification) -> CGFloat {
        tapGestureRecognizer.isEnabled = false
        return 0
    }
}
