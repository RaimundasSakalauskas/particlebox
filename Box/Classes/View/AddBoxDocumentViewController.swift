//
// Created by Raimundas Sakalauskas on 09/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation
import UIKit

class AddBoxDocumentViewController : BoxDocumentEditBaseViewController {

    //TODO: Add nice instruction on above table view (using tableview header)

    override var cells:[[CellType]]! {
        return [[CellType.key, CellType.value], [CellType.scope, CellType.deviceId, CellType.productId]]
    }

    private var callback: ((Bool) -> ())!
    private var boxDocument: BoxDocument!

    // MARK: Setup
    func setup(callback: @escaping (Bool) -> ()) {
        self.callback = callback
        self.boxDocument = BoxDocument(key: "", value: "", deviceId: nil, updatedAt: nil, scope: nil, productId: nil)
    }


    // MARK: Exit
    @IBAction func cancelPressed(_ sender: Any?) {
        view.endEditing(true)

        self.callback(false)
        dismiss(animated: true)
    }
    
    @IBAction func savePressed(_ sender: Any?) {
        view.endEditing(true)

        if (boxDocument.key.count == 0){
            scrollAndShake(target: IndexPath(row: 0, section: 0)) //key
        } else if (boxDocument.value.count == 0){
            scrollAndShake(target: IndexPath(row: 1, section: 0)) //value
        } else {
            showLoader()
            uploadBox()
        }
    }

    func scrollAndShake(target: IndexPath) {
        (tableView.cellForRow(at: target) as? InputCell)?.inputText.shake()
        //TODO: implement this
        //CGRect cellFrame = [self.tableView rectForRowAtIndexPath:targetIndexPath];
        //if (cellFrame.origin.y<self.tableView.contentOffset.y) { // the row is above visible rect
        //    [UIView animateWithDuration:0.25f animations:^{
        //        [self.tableView scrollToRowAtIndexPath:targetIndexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
        //    }                completion:^(BOOL finished) {
        //        [[self.tableView cellForRowAtIndexPath:targetIndexPath] shake];
        //    }];
        //}
        //else if(cellFrame.origin.y+cellFrame.size.height>self.tableView.contentOffset.y+self.tableView.frame.size.height-self.tableView.contentInset.top-self.tableView.contentInset.bottom){ // the row is below visible rect
        //    [UIView animateWithDuration:0.25f animations:^{
        //        [self.tableView scrollToRowAtIndexPath:targetIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        //    }                completion:^(BOOL finished) {
        //        [[self.tableView cellForRowAtIndexPath:targetIndexPath] shake];
        //    }];
        //} else {
        //    [[self.tableView cellForRowAtIndexPath:targetIndexPath] shake];
        //}
    }


    private func uploadBox() {
        boxDocument.updatedAt = Date() //update timestamp
        APIClient.instance.uploadBox(boxDocument: boxDocument) { response in
            switch response {
            case .success(_, _):
                self.callback(true)
                self.dismiss(animated: true)
            case .failure(let code, let message):
                self.showNetworkError(code: code, message: message,
                        retry: {
                            self.uploadBox()
                        },
                        cancel: {
                            self.hideLoader()
                        })
            }
        }
    }

    // MARK: TableView Data Source
    override func getValueForCell(type: CellType) -> Any? {
        switch type {
        case .key:
            return boxDocument.key
        case .value:
            return boxDocument.value
        case .deviceId:
            return boxDocument.deviceId
        case .productId:
            return boxDocument.productId
        case .scope:
            return boxDocument.scope
        }
    }

    override func setValueForCell(value: Any?, type: CellType) {
        switch (type) {
        case .key:
            boxDocument.key = (value as! String?) ?? ""
        case .value:
            boxDocument.value = (value as! String?) ?? ""
        case .deviceId:
            boxDocument.deviceId = value as! String?
        case .productId:
            let stringValue = value as! String?
            boxDocument.productId =   (stringValue == nil) ? nil : Int(stringValue!)
        case .scope:
            boxDocument.scope = value as! BoxDocumentScope?
            let stringValue = (boxDocument.scope == nil) ?
                    LocalizedString.BoxDocument.Unknown.localized() :
                    boxDocument.scope!.rawValue.capitalized

            let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 1)) as! ListCell //scope cell
            cell.valueLabel.text = stringValue
        }
    }

    // MARK: TableView Delegate
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 1:
            return LocalizedString.AddBox.Optional.localized()
        default:
            return ""
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 50
        case 1:
            return 30
        default:
            return 0
        }
    }
}
