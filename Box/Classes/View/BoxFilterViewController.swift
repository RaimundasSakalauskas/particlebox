//
// Created by Raimundas Sakalauskas on 11/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation

class BoxFilterViewController : BoxDocumentEditBaseViewController {

    override var cells:[[CellType]]! {
        return [[CellType.key, CellType.scope, CellType.deviceId, CellType.productId]]
    }


    // MARK: Display
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(filterCleared), name: .FilterCleared, object: nil)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .FilterCleared, object: nil)
    }

    @objc
    func filterCleared() {
        self.tableView.reloadData()
    }

    // MARK: Exit
    @IBAction func donePressed(_ sender: Any) {
        view.endEditing(true)
        ContainerViewController.instance.hideFilter()
    }

    // MARK: TableView DataSource
    override func getValueForCell(type: CellType) -> Any? {
        switch type {
        case .key:
            return ActiveFilter.instance.filter
        case .deviceId:
            return ActiveFilter.instance.deviceId
        case .productId:
            return ActiveFilter.instance.productId
        case .scope:
            return ActiveFilter.instance.scope
        default:
            return nil
        }
    }

    override func setValueForCell(value: Any?, type: CellType) {
        switch (type) {
        case .key:
            ActiveFilter.instance.filter = value as! String?
        case .deviceId:
            ActiveFilter.instance.deviceId = value as! String?
        case .productId:
            let stringValue = value as! String?
            ActiveFilter.instance.productId =  (stringValue == nil) ? nil : Int(stringValue!)
        case .scope:
            ActiveFilter.instance.scope = value as! BoxDocumentScope?
            let stringValue = (ActiveFilter.instance.scope == nil) ?
                    LocalizedString.BoxDocument.Unknown.localized() :
                    ActiveFilter.instance.scope!.rawValue.capitalized

            let cell = tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! ListCell //scope cell
            cell.valueLabel.text = stringValue
        default:
            //do nothing
            break
        }
    }

    // MARK: TableView Delegate
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}
