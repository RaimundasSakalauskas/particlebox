//
// Created by Raimundas Sakalauskas on 14/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation

class BoxDocumentEditBaseViewController : BaseViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!

    var cells:[[CellType]]! {
        fatalError("Property cells getter not implemented")
    }

    enum CellType : Int {
        case key
        case value
        case scope
        case deviceId
        case productId
    }

    // MARK: Setup
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupTableView()
    }

    func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self

        tableView.separatorColor = UIColor.BoxColor.greySeparator
        tableView.backgroundColor = UIColor.BoxColor.greyBackground

        tableView.register(UINib.init(nibName: InputCell.nibName, bundle: Bundle.main),
                forCellReuseIdentifier: InputCell.cellIdentifier)
        tableView.register(UINib.init(nibName: ListCell.nibName, bundle: Bundle.main),
                forCellReuseIdentifier: ListCell.cellIdentifier)
    }

    // MARK: Display
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        registerForKeyboardNotifications()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        removeKeyboardNotifications()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)

        if (segue.identifier == "ShowScope") {
            let vc = segue.destination as! BoxDocumentScopeListViewController
            vc.setup(currentScope: getValueForCell(type: .scope) as! BoxDocumentScope?) { scope in
                self.setValueForCell(value: scope, type: .scope)
            }
        }
    }

    // MARK: TableView DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return cells.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells[section].count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType = cells[indexPath.section][indexPath.row]

        var cellValue = getValueForCell(type: cellType)
        let cellPlaceHolder = getPlaceholderForCell(type: cellType)
        let cellTitle = getTitleForCell(type: cellType)

        if (cellType == .productId) {
            if (cellValue != nil) {
                cellValue = String(cellValue as! Int)
            }
        }

        switch (cellType) {
        case .key, .value, .deviceId, .productId:
            let cell:InputCell! = tableView.dequeueReusableCell(withIdentifier: InputCell.cellIdentifier, for: indexPath) as! InputCell
            cell.setup(titleKey: cellTitle, placeholderKey: cellPlaceHolder!)
            cell.inputText.tag = cellType.rawValue + 1
            cell.inputText.delegate = self
            cell.inputText.keyboardType = .default
            cell.inputText.text = cellValue as! String?
            return cell
        case .scope:
            let cell:ListCell! = tableView.dequeueReusableCell(withIdentifier: ListCell.cellIdentifier, for: indexPath) as! ListCell
            if (cellValue == nil) {
                cellValue = LocalizedString.AddBox.None.localized()
            } else {
                cellValue = (cellValue as! BoxDocumentScope).rawValue.capitalized
            }
            cell.setup(titleKey: cellTitle, value: cellValue as! String)
            cell.accessoryType = .disclosureIndicator
            return cell
        }
    }


    private func getTitleForCell(type: CellType) -> String  {
        switch type {
        case .key:
            return LocalizedString.BoxDocument.Key
        case .value:
            return LocalizedString.BoxDocument.Value
        case .deviceId:
            return LocalizedString.BoxDocument.DeviceId
        case .productId:
            return LocalizedString.BoxDocument.ProductId
        case .scope:
            return LocalizedString.BoxDocument.Scope
        }

    }

    private func getPlaceholderForCell(type: CellType) -> String? {
        switch type {
            case .key:
                return LocalizedString.BoxDocument.KeyPlaceholder
            case .value:
                return LocalizedString.BoxDocument.ValuePlaceholder
            case .deviceId:
                return LocalizedString.BoxDocument.DeviceIdPlaceholder
            case .productId:
                return LocalizedString.BoxDocument.ProductIdPlaceHolder
            case .scope:
                return nil
        }
    }

    func getValueForCell(type: CellType) -> Any? {
        fatalError("getValueForCell not implemented")
    }

    func validateValueForCell(value: String?, type:CellType) -> Bool {
        fatalError("setValueForCell not implemented")
    }

    func setValueForCell(value:Any?, type: CellType) {
        fatalError("setValueForCell not implemented")
    }



    // MARK: TableView Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "ShowScope", sender: indexPath)
    }

    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        //only allow selecting scope cell
        return cells[indexPath.section][indexPath.row] == .scope
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cells[indexPath.section][indexPath.row] == .scope ? ListCell.height : InputCell.height
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return cells[indexPath.section][indexPath.row] == .scope ? ListCell.height : InputCell.height
    }

    //MARK: Keyboard
    override func keyboardWillShow(_ notification: Notification) -> CGFloat {
        let height = super.keyboardWillShow(notification)
        tableView.contentInset.bottom = height + 10
        return height
    }

    override func keyboardWillHide(_ notification: Notification) -> CGFloat {
        let height = super.keyboardWillHide(notification)
        tableView.contentInset.bottom = height
        return height
    }


    //MARK: Keyboard
//    override func textFieldDidEndEditing(_ textField: UITextField) {
//        super.textFieldDidEndEditing(textField)
//
//        var textFieldValue = textField.text
//        if let value = textFieldValue, value.count == 0 {
//            textFieldValue = nil
//        }
//
//        let cellType = CellType(rawValue: textField.tag - 1)!
//        setValueForCell(value: textFieldValue, type: cellType)
//    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text: NSString = (textField.text ?? "") as NSString
        var resultString = text.replacingCharacters(in: range, with: string) as String?

        if resultString!.count == 0 {
            resultString = nil
        }

        let cellType = CellType(rawValue: textField.tag - 1)!
        var isValid = false

        if (resultString == nil) {
            isValid = true
        } else if (textField.tag == CellType.productId.rawValue + 1) {
            isValid = InputValidators.validateInt(input: resultString!)
        } else {
            isValid = InputValidators.validateString(input: resultString!)
        }

        if isValid {
            setValueForCell(value: resultString, type: cellType)
        }

        return isValid
    }

}
