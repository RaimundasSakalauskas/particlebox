//
// Created by Raimundas Sakalauskas on 09/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation
import UIKit

class BoxDocumentViewController : BaseViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var keyLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var deviceIdLabel: UILabel!
    @IBOutlet weak var updatedAtLabel: UILabel!
    @IBOutlet weak var productIdLabel: UILabel!
    @IBOutlet weak var scopeLabel: UILabel!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var tipLabel: UILabel!
    
    private var boxDocument: BoxDocument!
    private var callback: ((Bool, BoxDocument) -> ())!

    private var refreshControl: UIRefreshControl!
    private var apiTask: URLSessionDataTask?

    private var deleted: Bool = false
    private var skipCallback: Bool = false

    // MARK: Setup
    func setup(boxDocument: BoxDocument, callback: @escaping (Bool, BoxDocument) -> ()) {
        self.callback = callback
        self.boxDocument = boxDocument
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupScrollView()
        setupBoxDocument()
    }

    private func setupBoxDocument() {
        keyLabel.text = boxDocument.key.capitalized
        valueLabel.text = boxDocument.value

        if let deviceId = boxDocument.deviceId {
            deviceIdLabel.text = deviceId
        } else {
            deviceIdLabel.text = LocalizedString.BoxDocument.Unknown.localized()
        }

        if let date = boxDocument.updatedAt {
            updatedAtLabel.text = DateFormatter.localizedString(from: date,
                    dateStyle: Date().timeIntervalSince(date) < 43200 ? .none : .short, //if less than 12 hours ago
                    timeStyle: .medium)
        } else {
            updatedAtLabel.text = LocalizedString.BoxDocument.Unknown.localized()
        }

        if let productId = boxDocument.productId {
            productIdLabel.text = String(productId)
        } else {
            productIdLabel.text = LocalizedString.BoxDocument.Unknown.localized()
        }

        if let scope = boxDocument.scope {
            scopeLabel.text = scope.rawValue.uppercased()
        } else {
            scopeLabel.text = LocalizedString.BoxDocument.Unknown.localized()
        }
    }

    private func setupScrollView() {
        //refresh control setup
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self,
                action: #selector(handleRefresh(_:)),
                for: UIControlEvents.valueChanged
        )
        refreshControl.tintColor = UIColor.BoxColor.blueDark
        refreshControl.attributedTitle = NSAttributedString(string: LocalizedString.List.PullToRefresh.localized(),
                attributes: [NSAttributedStringKey.foregroundColor: UIColor.BoxColor.greyDarkText]
        )

        scrollView.addSubview(refreshControl)
    }


    // MARK: Display
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadBox()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if (apiTask != nil) {
            showLoader()
        }

        //Show tip UX tip
        UIView.animate(withDuration: 0.25, delay: 1, options: [],
        animations: {
            self.tipLabel.alpha = 1
        },
        completion: { b in
            UIView.animate(withDuration: 0.25, delay: 5, animations: {
                self.tipLabel.alpha = 0
            })
        })

        NotificationCenter.default.addObserver(self, selector: #selector(handleForcedReload), name: Notification.Name.ViewShouldBeReloaded, object: nil)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        NotificationCenter.default.removeObserver(self)
    }

    override func willMove(toParentViewController parent: UIViewController?) {
        super.willMove(toParentViewController: parent)

        if (parent == nil) {
            callback(deleted, boxDocument)
        }
    }

    override func hideLoader() {
        super.hideLoader()
        self.refreshControl.endRefreshing()
    }

    func showCopiedIndicator() {
        let copiedView = MBProgressHUD.showAdded(to: view, animated: true)
        copiedView.mode = MBProgressHUDMode.text
        copiedView.contentColor = UIColor.white
        copiedView.label.text = LocalizedString.BoxDocument.Copied.localized()

        copiedView.bezelView.style = MBProgressHUDBackgroundStyle.solidColor
        copiedView.bezelView.color = UIColor.BoxColor.blueDark
        copiedView.hide(animated: true, afterDelay: 1)
        copiedView.isUserInteractionEnabled = false
    }

    // MARK: Refresh
    @objc
    func handleRefresh(_ refreshControl: UIRefreshControl? = nil) {
        reloadBox()
    }

    @objc
    func handleForcedReload() {
        showLoader()
        reloadBox()
    }

    // MARK: Data Manipulation
    private func reloadBox() {
        if (apiTask != nil) {
            //page loading already in progress
            DLog("Api call already in progress")
            return
        }

        apiTask = APIClient.instance.getBox(boxDocument: boxDocument) { response in
            switch response {
            case .success(_, let data):
                guard let data = data else {
                    fatalError("Failed to retrieve data from API")
                }

                self.apiTask = nil
                self.boxLoaded(data)
            case .failure(let code, let message):
                self.showNetworkError(code: code, message: message,
                        retry: {
                            self.reloadBox()
                        },
                        cancel: {
                            self.apiTask = nil
                            self.hideLoader()
                        })
            }
        }
    }

    private func boxLoaded(_ data: Data) {
        let newBox = BoxDocument.decode(data: data)

        DLog("Reloaded Box: \(newBox)")
        boxDocument = newBox

        DispatchQueue.main.async { () -> Void in
            self.hideLoader()
            self.setupBoxDocument()
        }
    }



    @IBAction func removePressed(_ sender: Any) {
        //TODO: If adding iPad support, add sourcerect for popoverPresentationController
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: LocalizedString.Actions.DeleteBox.localized(),
                style: .destructive, handler: { action in

            self.deleted = true
            DispatchQueue.main.async { () -> Void in
                self.navigationController!.popViewController(animated: true)
            }
        }))

        alert.addAction(UIAlertAction(title: LocalizedString.Actions.Cancel.localized(),
                style: .cancel, handler: nil))

        self.present(alert, animated: true)
    }
    
    @IBAction func copyScopePressed(_ sender: Any) {
        UIPasteboard.general.string = scopeLabel.text!
        showCopiedIndicator()
    }
    
    @IBAction func copyDevicePressed(_ sender: Any) {
        UIPasteboard.general.string = deviceIdLabel.text!
        showCopiedIndicator()
    }
    
    @IBAction func copyProductPressed(_ sender: Any) {
        UIPasteboard.general.string = productIdLabel.text!
        showCopiedIndicator()
    }


    
}
