//
// Created by Raimundas Sakalauskas on 14/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation
import UIKit

class FilterFooterView : BlueView {
    @IBOutlet weak var clearButton: WhiteButton!
    @IBOutlet weak var activeFilterCount: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        NotificationCenter.default.addObserver(self, selector: #selector(update), name: .FilterCleared, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(update), name: .FilterChanged, object: nil)

        update()
    }

    @objc
    public func update() {
        let count = ActiveFilter.instance.count

        clearButton.isEnabled = count > 0
        activeFilterCount.text = "\(count)"
    }

    @IBAction func clearPressed(_ sender: Any) {
        ActiveFilter.instance.clear()
    }
    
    public func disable() {
        clearButton.isEnabled = false
    }
}
