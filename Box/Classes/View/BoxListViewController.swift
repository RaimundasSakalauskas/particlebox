//
//  BoxListViewController.swift
//  Box
//
//  Created by Raimundas Sakalauskas on 07/04/2018.
//  Copyright © 2018 Raimundas Sakalauskas. All rights reserved.
//

import UIKit
import Dispatch

class BoxListViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var noResultsView: UIView!
    @IBOutlet weak var noBoxesView: UIView!

    @IBOutlet weak var footerView: FilterFooterView!

    private var refreshControl: UIRefreshControl!
    private var boxes:[BoxDocument] = []

    private var pagesLoaded:Int = -1
    private var pagesTotal:Int = -1

    //used when returning from addBox view and on initial show
    private var reloadListOnAppear = true
    private var apiTask: URLSessionDataTask?
    private var pendingPageLoad: Bool = false



    // MARK: Setup
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
        hideMessages()
        hideFooter(instant: true)
    }

    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self

        tableView.separatorColor = UIColor.BoxColor.greySeparator
        tableView.backgroundColor = UIColor.BoxColor.greyBackground

        tableView.register(UINib.init(nibName: BoxDocumentCell.nibName, bundle: Bundle.main),
                forCellReuseIdentifier: BoxDocumentCell.cellIdentifier)
        tableView.register(UINib.init(nibName: LoadingCell.nibName, bundle: Bundle.main),
                forCellReuseIdentifier: LoadingCell.cellIdentifier)

        //refresh control setup
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self,
                action: #selector(handleRefresh(_:)),
                for: UIControlEvents.valueChanged
        )
        refreshControl.tintColor = UIColor.BoxColor.blueDark
        refreshControl.attributedTitle = NSAttributedString(string: LocalizedString.List.PullToRefresh.localized(),
                attributes: [NSAttributedStringKey.foregroundColor: UIColor.BoxColor.greyDarkText]
        )

        tableView.addSubview(refreshControl)
    }

    // MARK: Display
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        NotificationCenter.default.addObserver(self, selector: #selector(handleForcedReload), name: Notification.Name.ViewShouldBeReloaded, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleForcedReload), name: Notification.Name.FilterCleared, object: nil)

        if (reloadListOnAppear) {
            showLoader()

            reloadList()
            reloadListOnAppear = false
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        NotificationCenter.default.removeObserver(self)
    }

    private func reloadList() {
        pagesLoaded = -1
        loadNextPage()
    }

    private func hideMessages() {
        noResultsView.isHidden = true
        noBoxesView.isHidden = true
    }

    private func showMessage() {
        if (ActiveFilter.instance.count > 0) {
            noBoxesView.isHidden = true
            noResultsView.isHidden = false
        } else {
            noBoxesView.isHidden = false
            noResultsView.isHidden = true
        }
    }

    override func showLoader() {
        super.showLoader()
        view.addSubview(footerView)
    }

    override func disable() {
        super.disable()
        footerView.disable()
    }

    override func hideLoader() {
        super.hideLoader()
        self.refreshControl.endRefreshing()
        self.footerView.update()

        if (self.boxes.count == 0) {
            self.showMessage()
        } else {
            self.hideMessages()
        }
    }

    private func hideFooter(instant: Bool = false) {
        DispatchQueue.main.async {
            if (instant) {
                self.footerView.alpha = 0
                self.tableViewBottomConstraint.constant = 0
            } else {
                self.tableViewBottomConstraint.constant = 0
                //delay is required to workaround mbprogresshud bug
                UIView.animate(withDuration: 0.25, delay: 0.1, animations: {
                    self.footerView.alpha = 0
                    self.view.layoutIfNeeded()
                })
            }
        }
    }

    private func showFooter() {
        DispatchQueue.main.async {
            self.tableViewBottomConstraint.constant = self.footerView.frame.height

            UIView.animate(withDuration: 0.25) { () -> Void in
                self.footerView.alpha = 1.0
                self.view.layoutIfNeeded()
            }
        }
    }

    private func showDeleteAlertForBox(at indexPath: IndexPath) {
        //TODO: If adding iPad support, add sourcerect for popoverPresentationController
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: LocalizedString.Actions.DeleteBox.localized(),
                style: .destructive, handler: { action in

            DispatchQueue.main.async { () -> Void in
                self.deleteBox(at: indexPath)
            }
        }))

        alert.addAction(UIAlertAction(title: LocalizedString.Actions.Cancel.localized(),
                style: .cancel, handler: nil))

        self.present(alert, animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)

        if (segue.identifier == "ShowBox") {
            let indexPath = sender as! IndexPath
            let box = boxes[indexPath.row]

            let vc = segue.destination as! BoxDocumentViewController
            vc.setup(boxDocument: box) {
                deleted, box in
                if (deleted) {
                    self.deleteBox(at: indexPath)
                } else {
                    self.boxes[indexPath.row] = box
                    self.tableView.reloadRows(at: [indexPath], with: .none)
                }
            }
        } else if (segue.identifier == "AddBox") {
            let nc = segue.destination as! UINavigationController
            let vc = nc.viewControllers[0] as! AddBoxDocumentViewController
            vc.setup {
                success in
                if (success) {
                    self.reloadListOnAppear = true
                }
            }
        }
    }

    // MARK: Data manipulation
    private func loadNextPage() {
        pendingPageLoad = true

        //if refreshing whole list, cancel previous api calls
        if pagesLoaded == -1 {
            apiTask?.cancel()
            apiTask = nil
        } else if (apiTask != nil) {
            //page loading already in progress
            DLog("Api call already in progress")
            return
        }

        apiTask = APIClient.instance.listBoxes(
                scope: ActiveFilter.instance.scope?.rawValue,
                deviceId: ActiveFilter.instance.deviceId,
                productId: ActiveFilter.instance.productId != nil ? "\(ActiveFilter.instance.productId!)" : nil,
                filter: ActiveFilter.instance.filter,
                page: (pagesLoaded == -1) ? 1 : pagesLoaded + 1,
                perPage: Settings.instance.boxesPerPage)
        { response in
            switch response {
                case .success(_, let data):
                    guard let data = data else {
                        fatalError("Failed to retrieve data from API")
                    }

                    self.apiTask = nil
                    self.pageDataLoaded(data)
                    self.pendingPageLoad = false
                case .failure(let code, let message):
                    self.showNetworkError(code: code, message: message,
                    retry: {
                        self.loadNextPage()
                    },
                    cancel: {
                        self.apiTask = nil
                        self.hideLoader()

                    })
            }
        }
    }

    private func pageDataLoaded(_ data: Data) {

        let pagedBoxDocumentList = PagedBoxDocumentList.decode(data: data)

        //if loading the first page, remove all the elements
        if (pagesLoaded == -1) {
            boxes = pagedBoxDocumentList.data
        } else {
            boxes = boxes + pagedBoxDocumentList.data
        }

        if (Settings.instance.addFakeBoxesToEachPage) {
            boxes = boxes + pagedBoxDocumentList.data + pagedBoxDocumentList.data
            boxes = boxes + pagedBoxDocumentList.data + pagedBoxDocumentList.data
            boxes = boxes + pagedBoxDocumentList.data + pagedBoxDocumentList.data
            boxes = boxes + pagedBoxDocumentList.data + pagedBoxDocumentList.data
        }

        DLog("Boxes loaded: \(boxes.count)")

        pagesLoaded = pagedBoxDocumentList.meta.page
        pagesTotal = pagedBoxDocumentList.meta.total

        DispatchQueue.main.async { () -> Void in
            self.hideLoader()
            self.tableView.reloadData()
        }

    }


    private func loadNextBox() {
        //this is the last page, therefore there are no more items to load
        guard pagesLoaded < pagesTotal else {
            DispatchQueue.main.async { () -> Void in
                //it is possible that delete action triggered a page load that need to be executed
                if (!self.pendingPageLoad) {
                    self.hideLoader()
                } else {
                    self.loadNextPage()
                }
            }
            return
        }

        DLog("Boxes before loading last element: \(boxes.count)")
        apiTask = APIClient.instance.listBoxes(
                scope: ActiveFilter.instance.scope?.rawValue,
                deviceId: ActiveFilter.instance.deviceId,
                productId: ActiveFilter.instance.productId != nil ? "\(ActiveFilter.instance.productId!)" : nil,
                filter: ActiveFilter.instance.filter,
                page: boxes.count + 1,
                perPage: 1)
        { response in
            switch response {
            case .success(_, let data):
                guard let data = data else {
                    fatalError("Failed to retrieve data from API")
                }

                self.apiTask = nil
                self.boxDataLoaded(data)
            case .failure(let code, let message):
                self.showNetworkError(code: code, message: message,
                    retry: {
                        self.loadNextBox()
                    },
                    cancel: {
                        self.apiTask = nil
                        //it is possible that delete action triggered a page load that need to be executed
                        if (!self.pendingPageLoad) {
                            self.hideLoader()
                        } else {
                            self.loadNextPage()
                        }
                    })
            }
        }

    }

    private func boxDataLoaded(_ data: Data) {

        let pagedBoxDocumentList = PagedBoxDocumentList.decode(data: data)
        let tinyPageLoaded = pagedBoxDocumentList.meta.page
        let tinyPagesTotal = pagedBoxDocumentList.meta.total


        //if we loaded the last box on the list, we need to mark currently loaded
        //page as being the last on the list.
        if (tinyPageLoaded >= tinyPagesTotal) {
            pagesLoaded = pagesTotal
        }

        //if we actually loaded an element, append it to the list
        if (pagedBoxDocumentList.data.count > 0) {
            boxes = boxes + pagedBoxDocumentList.data
        }
        DLog("Boxes after loading last element: \(boxes.count)")

        DispatchQueue.main.async { () -> Void in
            //it is possible that delete action triggered a page load that need to be executed
            if (!self.pendingPageLoad) {
                self.hideLoader()
            } else {
                self.loadNextPage()
            }

            self.tableView.reloadData()
        }

    }

    private func deleteBox(at indexPath: IndexPath) {
        self.showLoader()
        self.deleteBox(self.boxes[indexPath.row])

        self.boxes.remove(at: indexPath.row)
        self.tableView.deleteRows(at: [indexPath], with: .fade)
    }

    private func deleteBox(_ boxToDelete: BoxDocument) {
        //the only task that can posibly be canceled here is "load next page" task which
        //should be canceled and cell removed
        if (apiTask != nil) {
            apiTask?.cancel()
            apiTask = nil

            //remove loading cell
            for cell in tableView.visibleCells {
                if (cell.isKind(of: LoadingCell.self)) {
                    tableView.deleteRows(at: [tableView.indexPath(for: cell)!], with: .fade)
                    break
                }
            }
        }

        apiTask = APIClient.instance.deleteBox(boxDocument: boxToDelete) { response in
            switch response {
            case .success(_, _):
                self.apiTask = nil
                self.loadNextBox()
            case .failure(let code, let message):
                self.showNetworkError(code: code, message: message,
                    retry: {
                        self.deleteBox(boxToDelete)
                    },
                    cancel: {
                        self.apiTask = nil
                        self.hideLoader()
                    })
            }
        }
    }

    @IBAction func addBoxPressed(_ sender: Any) {
        performSegue(withIdentifier: "AddBox", sender: nil)
    }
    
    @IBAction func showFilterPressed(_ sender: Any) {
        ContainerViewController.instance.showFilter()
    }
    // MARK: Refresh
    @objc
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        reloadList()
    }

    @objc
    func handleForcedReload() {
        showLoader()
        reloadList()

        if (ActiveFilter.instance.count > 0) {
            showFooter()
        } else {
            hideFooter()
        }
    }


    // MARK: TableView DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (Settings.instance.allowFakePaging) {
            if (boxes.count > 0) {
                return boxes.count + 1
            } else {
                return boxes.count
            }
        }

        //if there are more pages to load, make room for loading cell
        if (pagesLoaded < pagesTotal && boxes.count > 0) {
            return boxes.count + 1
        } else {
            return boxes.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row < boxes.count) {
            let cell = tableView.dequeueReusableCell(withIdentifier: BoxDocumentCell.cellIdentifier, for: indexPath) as! BoxDocumentCell
            cell.setup(boxDocument: boxes[indexPath.row])
            return cell
        } else {
            loadNextPage()
            return tableView.dequeueReusableCell(withIdentifier: LoadingCell.cellIdentifier, for: indexPath) as! LoadingCell
        }
    }


    // MARK: TableView Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "ShowBox", sender: indexPath)
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if (self.tableView(tableView, numberOfRowsInSection: section) > boxes.count) {
            return 1
        } else {
            return 40
        }
    }

    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        if (indexPath.row < boxes.count) {
            return true
        } else {
            return false
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row < boxes.count) {
            return BoxDocumentCell.height
        } else {
            return LoadingCell.height
        }
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row < boxes.count) {
            return BoxDocumentCell.height
        } else {
            return LoadingCell.height
        }
    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: LocalizedString.List.Delete.localized()) { (action, indexPath) in
            self.showDeleteAlertForBox(at: indexPath)
        }
        delete.backgroundColor = UIColor.BoxColor.red

        return [delete]
    }

}

