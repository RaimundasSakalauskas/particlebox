//
// Created by Raimundas Sakalauskas on 09/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation

struct PagedBoxDocumentList: Codable {

    var meta:Meta
    var data:[BoxDocument]

    struct Meta: Codable {
        var page: Int
        var total: Int
        var perPage: Int

        enum CodingKeys: String, CodingKey {
            case page = "page"
            case total = "total"
            case perPage = "per_page"
        }
    }

    enum CodingKeys: String, CodingKey {
        case meta = "meta"
        case data = "data"
    }

}

extension PagedBoxDocumentList {

    public static func decode(data: Data) -> PagedBoxDocumentList {
        do {
            return try APIClient.jsonDecoder.decode(PagedBoxDocumentList.self, from: data)
        } catch {
            fatalError("Failed to decode PagedBoxDocumentList")
        }
    }

}
