//
// Created by Raimundas Sakalauskas on 07/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation


enum BoxDocumentScope: String, Codable {
    case device
    case user
    case product

    init?(int:Int) {
        switch int {
            case 0:
                self = .device
            case 1:
                self = .user
            case 2:
                self = .product
            default:
                return nil
        }
    }

    public static var count: Int {
        return 3
    }

    public func toInt() -> Int {
        switch self {
            case .device:
                 return 0
            case .user:
                 return 1
            case .product:
                 return 2
        }
    }
}

struct BoxDocument: Codable, CustomStringConvertible {

    var key: String
    var value: String
    var deviceId: String?
    var updatedAt: Date?

    var scope: BoxDocumentScope?
    var productId: Int?

    enum CodingKeys: String, CodingKey {
        case key = "key"
        case value = "value"
        case scope = "scope"
        case deviceId = "device_id"
        case productId = "product_id"
        case updatedAt = "updated_at"
    }

    var description : String {
        get {
            return  """
                    {
                        key: \(key)
                        value: \(value)
                        scope: \(scope as Optional)
                        deviceId: \(deviceId as Optional)
                        productId: \(productId as Optional)
                        updatedAt: \(updatedAt as Optional)
                    }
                    """
        }
    }
}

extension BoxDocument {

    public static func decode(data: Data) -> BoxDocument {
        do {
            return try APIClient.jsonDecoder.decode(BoxDocument.self, from: data)
        } catch {
            fatalError("Failed to decode BoxDocument")
        }
    }

    public func encode() -> Data {
        do {
            return try APIClient.jsonEncoder.encode(self)
        } catch {
            fatalError("Unable to encode BoxDocument")
        }
    }

}



