//
// Created by Raimundas Sakalauskas on 07/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation


class Settings {

    static let instance = Settings()

    //must be more than fits in single screen of biggest device
    public var boxesPerPage: Int

    //used in add box and filter
    public var maxInputStringLength: Int

    //maximum time app can stay in the background without refreshing the views when
    //returning to foreground
    public var appBackgroundTimeBeforeViewRefresh: Double

    //helps with testing
    public var allowFakePaging: Bool
    public var addFakeBoxesToEachPage: Bool

    //helps with seeing what's happening on the API side
    public var devLogApiLevel: Int
    public var devLogEnabled: Bool

    init() {
        devLogApiLevel = 2
        devLogEnabled = true

        boxesPerPage = 20
        maxInputStringLength = 64

        appBackgroundTimeBeforeViewRefresh = 60

        allowFakePaging = false
        addFakeBoxesToEachPage = false

        IBARevealServerDisableAutoStart = true
    }




    public var IBARevealServerDisableAutoStart: Bool {
        get {
            if (UserDefaults.standard.object(forKey: "IBARevealServerDisableAutoStart") == nil) {
                return true
            }
            return UserDefaults.standard.bool(forKey: "IBARevealServerDisableAutoStart")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "IBARevealServerDisableAutoStart")
            UserDefaults.standard.synchronize()
        }
    }
}


public func DLog(_ message:String, usePrint:Bool = false) {
    if (Settings.instance.devLogEnabled) {
        if (usePrint) {
            print(message)
        } else {
            NSLog(message)
        }
    }
}
