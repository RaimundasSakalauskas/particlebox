//
// Created by Raimundas Sakalauskas on 09/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    struct BoxColor {
        static var blueDark: UIColor  { return UIColor(red: 0x04 / 255, green: 0x1e / 255, blue: 0x41 / 255, alpha: 1) }
        static var blueLight: UIColor  { return UIColor(red: 0x00 / 255, green: 0xae / 255, blue: 0xef / 255, alpha: 1) }
        static var blueIcon: UIColor  { return UIColor(red: 0x16 / 255, green: 0xad / 255, blue: 0xed / 255, alpha: 1) }

        static var greyBackground: UIColor  { return UIColor(red: 0xf5 / 255, green: 0xf5 / 255, blue: 0xf5 / 255, alpha: 1) }

        static var greyDarkText: UIColor  { return UIColor(red: 0x76 / 255, green: 0x77 / 255, blue: 0x7a / 255, alpha: 1) }
        static var greyText: UIColor  { return UIColor(red: 0x4a / 255, green: 0x4a / 255, blue: 0x4a / 255, alpha: 1) }
        static var greyMidLightText: UIColor  { return UIColor(red: 0x75 / 255, green: 0x75 / 255, blue: 0x75 / 255, alpha: 1) }
        static var greyLightText: UIColor  { return UIColor(red: 0x99 / 255, green: 0x99 / 255, blue: 0x99 / 255, alpha: 1) }
        static var greySeparator: UIColor  { return UIColor(red: 0xCC / 255, green: 0xCC / 255, blue: 0xCC / 255, alpha: 1) }


        static var red: UIColor  { return UIColor(red: 0xB3 / 255, green: 0x00 / 255, blue: 0x1B / 255, alpha: 1) }
    }
}

extension String {
    func localized() -> String {
        return LocalizedString.getLocalized(key: self)
    }
}


class BoxNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationBar.tintColor = UIColor.white
        navigationBar.backgroundColor = UIColor.BoxColor.blueDark

        navigationBar.shadowImage = UIImage()
        navigationBar.setBackgroundImage(UIImage(), for: .default)

        navigationBar.titleTextAttributes =
                [NSAttributedStringKey.foregroundColor: UIColor.white]

        //cover status bar
        view.addSubview(StatusBarBackgroundCoverView())

        //NSAttributedStringKey.font: UIFont(name: GrillFont.RobotoMedium, size: GrillFont.SemiLarge)

    }
}



class StatusBarBackgroundCoverView: UIView {
    private var statusBarHeightConstraint: NSLayoutConstraint?

    required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
    }

    override init(frame:CGRect) {
        super.init(frame:frame)

        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = UIColor.BoxColor.blueDark
    }

    convenience init() {
        self.init(frame: CGRect.zero)
    }

    override func didMoveToSuperview() {
        super.didMoveToSuperview()

        if (superview != nil) {
            leftAnchor.constraint(equalTo: superview!.leftAnchor).isActive = true
            rightAnchor.constraint(equalTo: superview!.rightAnchor).isActive = true
            topAnchor.constraint(equalTo: superview!.topAnchor).isActive = true

            if (statusBarHeightConstraint == nil) {
                statusBarHeightConstraint = heightAnchor.constraint(equalToConstant: 0)
                statusBarHeightConstraint!.isActive = true
            }
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        statusBarHeightConstraint?.constant = UIApplication.shared.statusBarFrame.height
    }
}

extension NSNotification.Name {
    public static let FilterCleared: NSNotification.Name = NSNotification.Name("NSNotification.Name.Box.FilterCleared")
    public static let FilterChanged: NSNotification.Name = NSNotification.Name("NSNotification.Name.Box.FilterChanged")
    public static let ViewShouldBeReloaded: NSNotification.Name = NSNotification.Name("NSNotification.Name.Box.ViewShouldBeReloaded")
}

extension UIView {

    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.3
        animation.values = [-15, 15, -7.5, 7.5, 0]
        self.layer.add(animation, forKey: "shake")
    }

}