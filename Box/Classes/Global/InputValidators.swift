//
// Created by Raimundas Sakalauskas on 11/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation

class InputValidators {
    static func validateString(input: String) -> Bool {
        return input.count <= Settings.instance.maxInputStringLength
    }

    static func validateInt(input: String) -> Bool {
        return Int(input) != nil
    }
}
