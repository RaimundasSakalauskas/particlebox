//
// Created by Raimundas Sakalauskas on 14/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation

class ActiveFilter {
    static let instance = ActiveFilter()

    private var filterString:String?
    public var filter:String? {
        set {
            filterString = newValue
            NotificationCenter.default.post(name: .FilterChanged, object: nil)
        }
        get {
            return filterString
        }
    }

    private var productIdInt:Int?
    public var productId:Int? {
        set {
            productIdInt = newValue
            NotificationCenter.default.post(name: .FilterChanged, object: nil)
        }
        get {
            return productIdInt
        }
    }

    private var deviceIdString:String?
    public var deviceId:String? {
        set {
            deviceIdString = newValue
            NotificationCenter.default.post(name: .FilterChanged, object: nil)
        }
        get {
            return deviceIdString
        }
    }

    private var scopeBoxDocumentScope: BoxDocumentScope?
    public var scope:BoxDocumentScope? {
        set {
            scopeBoxDocumentScope = newValue
            NotificationCenter.default.post(name: .FilterChanged, object: nil)
        }
        get {
            return scopeBoxDocumentScope
        }
    }


    public var count: Int {
        var temp = 0
        temp += (filterString != nil) ? 1 : 0
        temp += (productIdInt != nil) ? 1 : 0
        temp += (deviceIdString != nil) ? 1 : 0
        temp += (scopeBoxDocumentScope != nil) ? 1 : 0
        return temp
    }

    public func clear() {
        filterString = nil
        productId = nil
        deviceId = nil
        scope = nil

        NotificationCenter.default.post(name: .FilterCleared, object: nil)
    }
}
