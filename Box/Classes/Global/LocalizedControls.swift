//
// Created by Raimundas Sakalauskas on 09/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation
import UIKit

class LocalizedLabel : UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()

        text = text?.localized()
    }
}

class LocalizedButton : UIButton {

    @IBOutlet weak var contentView: UIView?

    override func awakeFromNib() {
        super.awakeFromNib()

        if let title = title(for: .normal) {
            setTitle(title.localized(), for: .normal)
        }

        self.addTarget(self, action: #selector(touchDown), for: .touchDown)
        self.addTarget(self, action: #selector(touchUp), for: .touchUpInside)
        self.addTarget(self, action: #selector(touchUp), for: .touchUpOutside)
        self.addTarget(self, action: #selector(touchUp), for: .touchCancel)
    }



    @objc
    func touchDown() {
        if let contentView = contentView {
            contentView.alpha = 0.25
        }
    }

    @objc
    func touchUp() {
        UIView.animate(withDuration: 0.5) {
            if let contentView = self.contentView {
                contentView.alpha = 1
            }
        }
    }

}


class LocalizedBarButtonItem : UIBarButtonItem {
    override func awakeFromNib() {
        super.awakeFromNib()

        if let title = title {
            self.title = title.localized()
        }
    }
}
