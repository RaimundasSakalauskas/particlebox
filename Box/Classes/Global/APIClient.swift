//
// Created by Raimundas Sakalauskas on 07/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation

enum APIResponse {
    case success(code: Int, response: Data?)
    case failure(code: Int, message: String?)
}


class APIClient {
    static let instance = APIClient()

    private let accessToken = "<access_token>"
    private var baseURL = "https://virtserver.swaggerhub.com/particle-iot/box/0.1"

    private var configuration: URLSessionConfiguration
    private var session: URLSession

    typealias CallbackType = (APIResponse)->Void
    
    private enum HTTPMethod: String {
        case POST = "POST"
        case GET = "GET"
        case DELETE = "DELETE"
    }

    init() {
        configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = [
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": "Bearer \(accessToken)"
        ]

        session = URLSession(configuration: configuration)
    }


    @discardableResult
    private func getTask(method:HTTPMethod, resource:String, body:Data? = nil, headerParams:[String: String]? = nil,
                         queryParams:[String: String]? = nil, callback: @escaping CallbackType) -> URLSessionDataTask {

        var queryString = ""
        if let queryParams = queryParams, queryParams.count > 0 {
            let paramsArray = queryParams.map { key, value -> String in
                return "\(key)=\(value.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)"
            }
            queryString = "?\(paramsArray.joined(separator: "&"))"
        }

        let url = URL(string: baseURL + resource + queryString)
        var urlRequest = URLRequest(url: url!)

        urlRequest.httpMethod = method.rawValue

        if let headerParams = headerParams {
            urlRequest.allHTTPHeaderFields = headerParams
        }

        if let bodyParams = body {
            urlRequest.httpBody = bodyParams
        }

        if (Settings.instance.devLogApiLevel == 2) {
            DLog("\(method.rawValue) \(resource + queryString)\n")
            DLog("API Client: Header Params: \(headerParams as Optional)", usePrint: true)
            if let body = body {
                DLog("API Client: Body Params: \(String(data: body, encoding: .utf8)!)", usePrint: true)
            } else {
                DLog("API Client: Body Params: None", usePrint: true)
            }
        } else if (Settings.instance.devLogApiLevel == 1) {
            DLog("\(method.rawValue) \(resource + queryString)")
        }

        let task = session.dataTask(with: urlRequest) {
            [weak self] (data: Data?, response: URLResponse?, error: Error?) -> Void in
            if let ss = self {
                ss.handleResponse(data: data, response: response, error: error, callback: callback);
            }
        }
        task.resume()

        return task
    }

    private func handleResponse(data: Data?, response: URLResponse?, error: Error?, callback: @escaping (APIResponse) -> Void) {

        guard let response = response, let httpResponse = response as? HTTPURLResponse else {
            callback(APIResponse.failure(code: -1, message: nil))

            if (Settings.instance.devLogApiLevel > 0) {
                DLog("API Client: Network error")
            }

            return
        }

        if (Settings.instance.devLogApiLevel == 2) {
            if let data = data {
                DLog("\(httpResponse.statusCode) \(httpResponse.url!.absoluteURL)")
                DLog(String(bytes: data, encoding: .utf8)!, usePrint: true)
            } else {
                DLog("\(httpResponse.statusCode) \(httpResponse.url!.absoluteURL)\nNo data")
            }
        }
        else if (Settings.instance.devLogApiLevel == 1) {
            DLog("\(httpResponse.statusCode) \(httpResponse.url!.path)")
        }


        if let error = error {
            callback(APIResponse.failure(code: httpResponse.statusCode, message: error.localizedDescription))
            return
        }

        //if data is non-nil but empty, then return nil
        var data = data
        if (data?.count == 0) {
            data = nil
        }

        if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 300) {
            callback(APIResponse.success(code: httpResponse.statusCode, response: data))
        } else {
            callback(APIResponse.failure(code: httpResponse.statusCode, message: nil))
        }
    }
}

extension APIClient {

    static var dateFormatter: ISO8601DateFormatter {
        get {
            let dateFormatter = ISO8601DateFormatter()
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            dateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
            return dateFormatter
        }
    }

    static var jsonEncoder: JSONEncoder {
        get {
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            encoder.dateEncodingStrategy = .custom { date, encoder in
                var container = encoder.singleValueContainer()
                try! container.encode(dateFormatter.string(from: date))
            }
            return encoder
        }
    }

    static var jsonDecoder: JSONDecoder {
        get {
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .custom { decoder in
                let container = try decoder.singleValueContainer()
                let dateString = try container.decode(String.self)

                if let date = dateFormatter.date(from: dateString) {
                    return date
                } else {
                    fatalError("Unable to decode date")
                }
            }

            return decoder
        }
    }



}

extension APIClient {

    //200: list documents matching criteria
    //400: bad input parameter
    @discardableResult
    func listBoxes(scope: String? = nil, deviceId: String? = nil, productId: String? = nil, filter: String? = nil, page:Int = 0,
                   perPage:Int = 10, callback: @escaping CallbackType) -> URLSessionDataTask {

        var params = [String: String]()
        params["page"] = String(page)
        params["per_page"] = String(perPage)

        if let scope = scope {
            params["scope"] = scope
        }

        if let deviceId = deviceId {
            params["device_id"] = deviceId
        }

        if let productId = productId {
            params["product_id"] = productId
        }

        if let filter = filter {
            params["filter"] = filter
        }

        return getTask(method: .GET, resource: "/box", queryParams: params, callback: callback);
    }

    //201: Document created
    //400: Invalid input, object invalid
    @discardableResult
    func uploadBox(boxDocument: BoxDocument, callback: @escaping CallbackType) -> URLSessionDataTask {
        return getTask(method: .POST, resource: "/box", body: boxDocument.encode(), callback: callback);
    }

    //204: document deleted
    //400: bad input parameter
    //404: no document exists for these criteria
    @discardableResult
    func getBox(boxDocument: BoxDocument, callback: @escaping CallbackType) -> URLSessionDataTask {

        var params = [String: String]()
        if let scope = boxDocument.scope {
            params["scope"] = scope.rawValue
        }

        if let deviceId = boxDocument.deviceId {
            params["device_id"] = deviceId
        }

        if let productId = boxDocument.productId {
            params["product_id"] = String(productId)
        }

        return getTask(method: .GET, resource: "/box/\(boxDocument.key.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)", queryParams: params.count > 0 ? params : nil, callback: callback);
    }

    //204: document deleted
    //400: bad input parameter
    //404: no document exists for these criteria
    @discardableResult
    func deleteBox(boxDocument: BoxDocument, callback: @escaping CallbackType) -> URLSessionDataTask {

        var params = [String: String]()
        if let scope = boxDocument.scope {
            params["scope"] = scope.rawValue
        }

        if let deviceId = boxDocument.deviceId {
            params["device_id"] = deviceId
        }

        if let productId = boxDocument.productId {
            params["product_id"] = String(productId)
        }

        
        return getTask(method: .DELETE, resource: "/box/\(boxDocument.key.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)", queryParams: params.count > 0 ? params : nil, callback: callback);
    }


}

