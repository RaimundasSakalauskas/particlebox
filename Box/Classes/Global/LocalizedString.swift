//
// Created by Raimundas Sakalauskas on 09/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation

struct LocalizedString {
    static func getLocalized(key: String) -> String {
        //if lanauge == english
        return BaseLanguage.getLocalized(key: key)
        //else
        //return GermanLanguage.getLocalized(key: key)
    }

    struct Errors {
        struct NetworkError {
            //NSLocalizedString("Errors.NetworkError.Title", value: "Huston, problem", comment: "Message title for network error")
            static var Title = "Errors.NetworkError.Title"

            //NSLocalizedString("Errors.NetworkError.Message", value: "Your last action failed to complete", comment: "Message text for network error")
            static var Message = "Errors.NetworkError.Message"
        }
    }

    struct Actions {
        //NSLocalizedString("Actions.DeleteBox", value: "Remove Box", comment: "Action for deleting box shown in action sheet")
        static var DeleteBox = "Actions.DeleteBox"

        //NSLocalizedString("Actions.Cancel", value: "Cancel", comment: "Cancel action")
        static var Cancel = "Actions.Cancel"

        //NSLocalizedString("Actions.Retry", value: "Retry", comment: "Retry action")
        static var Retry = "Actions.Retry"

        //NSLocalizedString("Actions.Add", value: "Add", comment: "Add action")
        static var Add = "Actions.Add"

        //NSLocalizedString("Actions.ClearFilters", value: "Clear Filters", comment: "Clear filters action")
        static var ClearFilters = "Actions.ClearFilters"
    }

    struct List {
        //NSLocalizedString("List.Title", value: "Boxes", comment: "Title for list view")
        static var Title = "List.Title"

        //NSLocalizedString("List.Delete", value: "Remove", comment: "Title shown when removing box document by swiping on the list element")
        static var Delete = "List.Delete"

        //NSLocalizedString("List.PullToRefresh", value: "Pull To Refresh", comment: "Instruction shown when box list is being dragged down from the top")
        static var PullToRefresh = "List.PullToRefresh"


        //NSLocalizedString("List.NoResults", value: "No results matching filter criteria.", comment: "Message shown when selected filters result in 0 search results")
        static var NoResults = "List.NoResults"

        //NSLocalizedString("List.NoBoxes", value: "No boxes on the list. Try adding some.", comment: "Message shown when list is empty with no filters applied.")
        static var NoBoxes = "List.NoBoxes"
    }

    struct BoxDocument {
        //NSLocalizedString("BoxDocument.DeviceId", value: "DEVICE ID", comment: "Label for box document deviceId property")
        static var DeviceId = "BoxDocument.DeviceId"

        //NSLocalizedString("BoxDocument.DeviceIdPlaceholder", value: "e.g. 1234-12345-1234", comment: "Placeholder text")
        static var DeviceIdPlaceholder = "BoxDocument.DeviceIdPlaceholder"

        //NSLocalizedString("BoxDocument.Scope", value: "SCOPE", comment: "Label for box document scope property")
        static var Scope = "BoxDocument.Scope"

        //NSLocalizedString("BoxDocument.ProductId", value: "PRODUCT ID", comment: "Label for box document productId property")
        static var ProductId = "BoxDocument.ProductId"

        //NSLocalizedString("BoxDocument.ProductIdPlaceHolder", value: "e.g. 1234", comment: "Placeholder text")
        static var ProductIdPlaceHolder = "BoxDocument.ProductIdPlaceHolder"

        //NSLocalizedString("BoxDocument.Key", value: "KEY", comment: "Label for box document key property")
        static var Key = "BoxDocument.Key"

        //NSLocalizedString("BoxDocument.KeyPlaceholder", value: "e.g. Temperature", comment: "Placeholder text")
        static var KeyPlaceholder = "BoxDocument.KeyPlaceholder"

        //NSLocalizedString("BoxDocument.Value", value: "VALUE", comment: "Label for box document value property")
        static var Value = "BoxDocument.Value"

        //NSLocalizedString("BoxDocument.ValuePlaceholder", value: "e.g. 25", comment: "Placeholder text")
        static var ValuePlaceholder = "BoxDocument.ValuePlaceholder"


        //NSLocalizedString("BoxDocument.UpdatedAt", value: "LAST UPDATED", comment: "Label for box document updatedAt property")
        static var UpdatedAt = "BoxDocument.UpdatedAt"

        //NSLocalizedString("BoxDocument.Unknown", value: "Unknown", comment: "Label for updatedAt field when property value is nil")
        static var Unknown = "BoxDocument.Unknown"

        //NSLocalizedString("BoxDocument.TapToCopy", value: "Tap on a detail to copy", comment: "Instruction label on bottom of document view")
        static var TapToCopy = "BoxDocument.TapToCopy"

        //NSLocalizedString("BoxDocument.Copied", value: "Copied!", comment: "Flash message text after document property is copied to clipboard")
        static var Copied = "BoxDocument.Copied"
    }


    struct Filter {
        //NSLocalizedString("Filter.Title", value: "Filter", comment: "Title for filters screen")
        static var Title = "Filter.Title"

        //NSLocalizedString("Filter.ActiveFilters", value: "Active Filters", comment: "Label for active filters count")
        static var ActiveFilters = "Filter.ActiveFilters"
    }

    struct AddBox {
        //NSLocalizedString("AddBox.Title", value: "New Box", comment: "Title for AddBox screen")
        static var Title = "AddBox.Title"

        //NSLocalizedString("AddBox.Required", value: "Required", comment: "Title for section in Add Box screen")
        static var Required = "AddBox.Required"

        //NSLocalizedString("AddBox.Optional", value: "Optional", comment: "Title for section in Add Box screen")
        static var Optional = "AddBox.Optional"

        //NSLocalizedString("BoxDocument.None", value: "None", comment: "Value for undefined scope property")
        static var None = "BoxDocument.None"

        //NSLocalizedString("BoxDocument.SelectScope", value: "Select Scope", comment: "Title for scope selection view")
        static var SelectScope = "BoxDocument.SelectScope"
    }


    class BaseLanguage {
        static var baseBundle: Bundle?

        class var bundle: Bundle? {
            return baseBundle
        }

        class var language: String {
            return "Base"
        }

        static func getLocalized(key: String) -> String {
            if (bundle == nil) {
                initBundle()
            }

            let string = bundle?.localizedString(forKey:key, value: nil, table: nil)
            return string!
        }

        internal class func initBundle() {
            let path = Bundle.main.path(forResource:language, ofType: "lproj")
            baseBundle = Bundle(path: path!)
        }
    }

//    class EnglishLanguage: BaseLanguage {
//        static var languageBundle: Bundle?
//
//        override class var bundle: Bundle? {
//            return languageBundle
//        }
//
//        override class var language: String {
//            return "en"
//        }
//
//        override internal class func initBundle() {
//            let path = Bundle.main.path(forResource:language, ofType: "lproj")
//            languageBundle = Bundle(path: path!)
//        }
//    }


//    class GermanLanguage: BaseLanguage {
//        static var languageBundle: Bundle?
//
//        override class var bundle: Bundle? {
//            return languageBundle
//        }
//
//        override class var language: String {
//            return "de"
//        }
//
//        override internal class func initBundle() {
//            let path = Bundle.main.path(forResource:language, ofType: "lproj")
//            languageBundle = Bundle(path: path!)
//        }
//    }

}


