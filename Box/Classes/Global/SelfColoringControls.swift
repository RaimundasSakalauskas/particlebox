//
// Created by Raimundas Sakalauskas on 09/04/2018.
// Copyright (c) 2018 Raimundas Sakalauskas. All rights reserved.
//

import Foundation
import UIKit

// MARK: Labels
class GreyLabel : SelfColoringLabel {
    override var color: UIColor {
        return UIColor.BoxColor.greyText
    }
}

class GreyLightLabel : SelfColoringLabel {
    override var color: UIColor {
        return UIColor.BoxColor.greyLightText
    }
}

class GreyMidLightLabel : SelfColoringLabel {
    override var color: UIColor {
        return UIColor.BoxColor.greyMidLightText
    }
}

class GreyDarkLabel : SelfColoringLabel {
    override var color: UIColor {
        return UIColor.BoxColor.greyDarkText
    }
}

class BlackLabel : SelfColoringLabel {
    override var color: UIColor {
        return .black
    }
}

class BlueLabel : SelfColoringLabel {
    override var color: UIColor {
        return UIColor.BoxColor.blueDark
    }
}

class BlueLightLabel : SelfColoringLabel {
    override var color: UIColor {
        return UIColor.BoxColor.blueLight
    }
}

class WhiteLabel : SelfColoringLabel {
    override var color: UIColor {
        return .white
    }
}

class SelfColoringLabel : LocalizedLabel {
    open var color: UIColor {
        return .black
    }

    func setup() {
        text = text?.localized()
        textColor = color
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        setup()
    }
}





// MARK: Views

class WhiteView : SelfColoringView {
    override var color: UIColor {
        return .white
    }
}

class GreyBackgroundView : SelfColoringView {
    override var color: UIColor {
        return UIColor.BoxColor.greyBackground
    }
}

class BlueView : SelfColoringView {
    override var color: UIColor {
        return UIColor.BoxColor.blueDark
    }
}

class RedView : SelfColoringView {
    override var color: UIColor {
        return UIColor.BoxColor.red
    }
}

class GrayBackgroundView : SelfColoringView {
    override var color: UIColor {
        return UIColor.BoxColor.greyBackground
    }
}

class GraySeparatorView : SelfColoringView {
    override var color: UIColor {
        return UIColor.BoxColor.greySeparator
    }
}


class SelfColoringView : UIView {
    open var color: UIColor {
        return .black
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setup()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        setup()
    }

    func setup() {
        backgroundColor = color
    }

    func reset() {
        backgroundColor = color
    }
}




//MARK: Buttons
class RedButton : SelfColoringButton {
    override var color: UIColor {
        return UIColor.BoxColor.red
    }
}

class WhiteButton : SelfColoringButton {
    override var color: UIColor {
        return UIColor.white
    }
}


class SelfColoringButton : LocalizedButton {
    open var color: UIColor {
        return UIColor.black
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        setup()
    }

    private func setup() {
        setTitleColor(color, for: .normal)
        setTitleColor(color.withAlphaComponent(0.3), for: .disabled)
    }
}



// MARK: TextField
class BlueDarkTextField : SelfColoringTextField {
    override var color: UIColor {
        return UIColor.BoxColor.blueDark
    }
}

class SelfColoringTextField : UITextField {
    open var color: UIColor {
        return .black
    }

    func setup() {
        textColor = color
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        setup()
    }
}