//
//  AppDelegate.swift
//  Box
//
//  Created by Raimundas Sakalauskas on 07/04/2018.
//  Copyright © 2018 Raimundas Sakalauskas. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var enterBackgroundDate:Date?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        enterBackgroundDate = Date()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {

        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        if let enterBackgroundDate = enterBackgroundDate {
            DLog("Date().timeIntervalSince(enterBackgroundDate) = \(Date().timeIntervalSince(enterBackgroundDate))")
            if (Date().timeIntervalSince(enterBackgroundDate) > Settings.instance.appBackgroundTimeBeforeViewRefresh) {
                NotificationCenter.default.post(name: Notification.Name.ViewShouldBeReloaded, object: nil)
            }
        }
        enterBackgroundDate = nil
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

